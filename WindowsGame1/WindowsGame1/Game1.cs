using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using WindowsGame1;

namespace WindowsGame1
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Thread cool;
        public int particlelimit;
        public int maxX = 0;
        public int maxY = 0;
        int psize = 10;
        public static int inum { set; get; }
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            
            Content.RootDirectory = "Content";
            Components.Add(new fpsCount(this));
            particlelimit = 1000;
            cool = new Thread(new ThreadStart(partgen));
            
        }

        
        
        protected override void Initialize()
        {
            maxX = GraphicsDevice.Viewport.Width;
            maxY = GraphicsDevice.Viewport.Height;
            this.IsMouseVisible = true;


            base.Initialize();
        }

       
        
        
        Parti[] particles = new Parti[1000];
        Random r = new Random();
        protected override void LoadContent()
        {
       
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            
            

            
   
        }

        protected override void UnloadContent()
        {
          
        }


        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            if (psize < 80 & Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                psize = psize + 4;
            }
            if (psize > 4 & Mouse.GetState().MiddleButton == ButtonState.Pressed)
            {
                psize = psize - 4;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                particles = new Parti[1000];
                inum = 0;
            }


                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    if (inum < particlelimit)
                    {
                        particles[inum] = new Parti(graphics.GraphicsDevice, Mouse.GetState().X, Mouse.GetState().Y, Color.Red, spriteBatch, maxX, maxY, psize);
                        inum++;
                    }
                }

                for (int pp = 0; pp < inum; pp++)
                {
                    
                    particles[pp].moveTO(r.Next(maxX), r.Next(maxY));
                    particles[pp].Update();
                }
                
                //Console.WriteLine(Mouse.GetState().X + " " + Mouse.GetState().Y+"   ")
                //partgen();

                base.Update(gameTime);
            
        }

      
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.Green);
           
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            
            if (particles[1] != null)
            {
                for (int p = 0; p < inum; p++)
                {
                    particles[p].Draw();

                }
            }
                
            spriteBatch.End();


           base.Draw(gameTime);
           
        }

        void partgen()
        {
            /*for (int i = 0; i < particles.Length; i++)
            {
                particles[i] = new Parti(graphics.GraphicsDevice, r.Next(maxX), r.Next(maxY), Color.Red, spriteBatch, maxX, maxY, psize);
            }
             */

            if (inum < particlelimit)
            {
                particles[inum] = new Parti(graphics.GraphicsDevice, Mouse.GetState().X, Mouse.GetState().Y, Color.Red, spriteBatch, maxX, maxY, psize);
                inum++;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace WindowsGame1

{
    class Parti
    {
        int size = 4;
        Vector2 pos = Vector2.Zero;
        SpriteBatch ssb;
        Texture2D texture;
        Color coloer { get; set; }
        int maxx, maxy = 0;
        public Parti(GraphicsDevice GD, int startx, int starty, Color col, SpriteBatch sb, int maxX, int maxY, int siz)
        {
            ssb = sb;
            coloer = col;
            maxx = maxX;
            maxy = maxY;
            size = siz;
            texture = new Texture2D(GD, size, size, false, SurfaceFormat.Color);
            Color[] pdata = new Color[size*size];
            for (int i = 0; i < pdata.Length; i++) pdata[i] = col;
            texture.SetData(pdata);
            pos.X = startx;
            pos.Y = starty;
        }
        public void Update()
        {
            if (this.pos.X+4 > maxx)
            {
                this.pos.X = maxx-4;
            }
            if (this.pos.X - 4 < 0)
            {
                this.pos.X = this.pos.X + 4;
            }
            if (this.pos.Y+4 > maxy)
            {
                this.pos.Y = maxy-4;
            }
            if (this.pos.Y-4 < 0)
            {
                this.pos.Y = this.pos.Y +1;
            }
            this.pos.Y = this.pos.Y + 1;
            
        }
        public void moveTO(int x, int y)
        {
            if (this.pos.X > x)
            {
                this.pos.X = this.pos.X -1;
            }
            if (this.pos.X  < x)
            {
                this.pos.X = this.pos.X + 1;
            }
                
                



        }
        public void Draw()
        {
            ssb.Draw(texture, pos, coloer); 
        }
    }
}
